﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.entities
{
    class Extintor
    {
        private int id;
        private string categoria;
        private string marca;
        private string capacidad;
        private string tipoextintor;
        private string unidadmedida;
        private string cantidad;

        public Extintor(int id, string categoria, string marca, string capacidad, string tipoextintor, string unidadmedida, string cantidad)
        {
            this.id = id;
            this.categoria = categoria;
            this.marca = marca;
            this.capacidad = capacidad;
            this.tipoextintor = tipoextintor;
            this.unidadmedida = unidadmedida;
            this.cantidad = cantidad;
        }

        public int Id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }

        public string Categoria
        {
            get
            {
                return categoria;
            }

            set
            {
                categoria = value;
            }
        }

        public string Marca
        {
            get
            {
                return marca;
            }

            set
            {
                marca = value;
            }
        }

        public string Capacidad
        {
            get
            {
                return capacidad;
            }

            set
            {
                capacidad = value;
            }
        }

        public string Tipoextintor
        {
            get
            {
                return tipoextintor;
            }

            set
            {
                tipoextintor = value;
            }
        }

        public string Unidadmedida
        {
            get
            {
                return unidadmedida;
            }

            set
            {
                unidadmedida = value;
            }
        }

        public string Cantidad
        {
            get
            {
                return cantidad;
            }

            set
            {
                cantidad = value;
            }
        }
    }
}


