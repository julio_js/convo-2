﻿using NETProjectTutorial.entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.model
{
    class ExtintorModel
    {
        private static  List<Extintor> productos = new List<Extintor>();


        public static List<Extintor> GetProductos()
        {
            return productos;
        }

        public static void Populate()
        {
            Extintor[] pdts =
            {
                new Extintor("Americano","Asiatico","110","CO2", "Libras", "10"),
                
            };

            productos = pdts.ToList();
            
        }
    }
}
