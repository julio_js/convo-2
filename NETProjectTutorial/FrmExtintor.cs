﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NETProjectTutorial.Properties;
using NETProjectTutorial.entities;

namespace NETProjectTutorial
{
    public partial class FrmExtintor : Form
    {
        private DataTable tblExtintores ;
        private DataSet dsExtintor;
        private BindingSource bsExtintor;
        private DataRow drExtintor;

        public FrmExtintor()
        {
            InitializeComponent();
            bsExtintor = new BindingSource();
        }

        public DataTable TblProductos { set { tblExtintores = value; } }
        public DataSet DsProductos { set { dsExtintor = value; } }
        public DataRow DrProducto
        {
            set {
                drExtintor = value;
                txtcategoria.Text = drExtintor["Categoria"].ToString();
                txtMarca.Text = drExtintor["Marca"].ToString();
                txtCapacidad.Text = drExtintor["Capacidad"].ToString();
                txtTipo.Text = drExtintor["Tipo_Extintor"].ToString();
                txtunidad.Text = drExtintor["Unidad_Medida"].ToString();
                txtcantidad.Text = drExtintor["Cantidad"].ToString();

            }
            
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            string categoria, marca, capacidad, tipoextintor, unidadmedida, cantidad;
            

            categoria = txtcategoria.Text;
            marca = txtMarca.Text;
            capacidad = txtCapacidad.Text;
            tipoextintor = txtTipo.Text;
            unidadmedida = txtunidad.Text;
            cantidad = txtcantidad.Text;


            if (drExtintor != null)
            {
                DataRow drNew = tblExtintores.NewRow();

                int index = tblExtintores.Rows.IndexOf(drExtintor);
                drNew["Id"] = drExtintor["Id"];
                drNew["Categoria"] = categoria;
                drNew["Marca"] = marca;
                drNew["Capacidad"] = capacidad;
                drNew["Tipo"] = tipoextintor;
                drNew["Medida"] = unidadmedida;
                drNew["Cantidad"] = cantidad;


                tblExtintores.Rows.RemoveAt(index);
                tblExtintores.Rows.InsertAt(drNew, index);               

            }
            else
            {
                tblExtintores.Rows.Add(tblExtintores.Rows.Count + 1, categoria, marca, capacidad, tipoextintor, unidadmedida, cantidad);
            }

            Dispose();

        }

        private void FrmProducto_Load(object sender, EventArgs e)
        {
            bsExtintor.DataSource = dsExtintor;
            bsExtintor.DataMember = dsExtintor.Tables["Extintores"].TableName;       
             

        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Dispose();
        }
    }
}
